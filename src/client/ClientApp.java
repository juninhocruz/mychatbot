package client;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ClientApp extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tfEmail;
	private JLabel lblEmail;
	private Registry registry;
	private Mensageiro gerente;
	
	private static ClientApp app;
	private static ChatApp chat;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					app = new ClientApp();
					app.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ClientApp() {
		String ip = "192.168.0.107";
		int port = 8080;
		String host = "MyChatBot";
		
		try {
			registry = LocateRegistry.getRegistry(ip, port);
			
			gerente = (Mensageiro) registry.lookup(host);
		} catch (RemoteException e) {
			JOptionPane.showMessageDialog(null, "001 - Erro ao conectar servidor.", "Algo deu errado!", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (NotBoundException e) {
			JOptionPane.showMessageDialog(null, "002 - Erro ao conectar servidor.", "Algo deu errado!", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 160);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblLogin = new JLabel("Login");
		panel.add(lblLogin);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2, BorderLayout.NORTH);
		
		lblEmail = new JLabel("Email:   ");
		panel_2.add(lblEmail);
		
		tfEmail = new JTextField();
		panel_2.add(tfEmail);
		tfEmail.setColumns(20);
		
		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3, BorderLayout.SOUTH);
		
		JLabel lblDesenvolvidoPorAntnio = new JLabel("Desenvolvido por: António Cruz Jr e Osmar Kabashima Jr");
		panel_3.add(lblDesenvolvidoPorAntnio);
		
		JPanel panel_4 = new JPanel();
		panel_1.add(panel_4, BorderLayout.CENTER);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_5 = new JPanel();
		panel_4.add(panel_5, BorderLayout.NORTH);
		
		JButton btnEntrar = new JButton("Entrar");
		panel_5.add(btnEntrar);
		btnEntrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				login();
			}
		});
	}
	
	private void login() {
		String name = tfEmail.getText();
		boolean isRegistered = false;
		try {
			isRegistered = gerente.register(name);
		} catch (RemoteException e) {
			JOptionPane.showMessageDialog(null, "003 - Erro ao obter resposta do servidor.", "Algo deu errado!", JOptionPane.ERROR_MESSAGE);
			System.out.println(e.getLocalizedMessage() + " | " + e.getMessage());
		}
		
		if(isRegistered)
			JOptionPane.showMessageDialog(null, "Login efetuado com sucesso!", "Login", JOptionPane.INFORMATION_MESSAGE);
		else
			JOptionPane.showMessageDialog(null, "Cadastro efetuado com sucesso!", "Signup", JOptionPane.INFORMATION_MESSAGE);
		
		chat = new ChatApp(registry, gerente, app, name);
		app.setVisible(false);
		chat.setVisible(true);
	}

	public void finishChat() {
		setVisible(true);
		if(chat != null)
			chat.dispose();
		chat = null;
	}
}
