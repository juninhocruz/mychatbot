package client;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface Mensageiro extends Remote {
	public String hello(String nome) throws RemoteException;
	
	public boolean register(String email) throws RemoteException;
	
	public String sendMessage(String email, String message) throws RemoteException;
	
	public ArrayList<String> getHistoric(String email) throws RemoteException;
}
