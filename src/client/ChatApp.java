package client;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ChatApp extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tfEmail;
	private JLabel lblEmail;
	private Mensageiro gerente;
	private JTextField tfChat;
	private ClientApp clientApp;
	private String email;
	private JTextArea textArea;

	/**
	 * Create the frame.
	 */
	public ChatApp(Registry registry, Mensageiro gerente, ClientApp clientApp, String email) {
		this.clientApp = clientApp;
		this.gerente = gerente;
		this.email = email;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 323);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblTitle = new JLabel("Chat");
		panel.add(lblTitle);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2, BorderLayout.NORTH);
		
		lblEmail = new JLabel("Email:   ");
		panel_2.add(lblEmail);
		
		tfEmail = new JTextField();
		tfEmail.setEditable(false);
		panel_2.add(tfEmail);
		tfEmail.setColumns(20);
		tfEmail.setText(email);
		
		JButton btnSair = new JButton("Sair");
		panel_2.add(btnSair);
		btnSair.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				logout();
			}
		});
		
		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3, BorderLayout.SOUTH);
		
		JLabel lblDesenvolvidoPorAntnio = new JLabel("Desenvolvido por: António Cruz Jr e Osmar Kabashima Jr");
		panel_3.add(lblDesenvolvidoPorAntnio);
		
		JPanel panel_4 = new JPanel();
		panel_1.add(panel_4, BorderLayout.CENTER);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_5 = new JPanel();
		panel_4.add(panel_5, BorderLayout.SOUTH);
		
		tfChat = new JTextField();
		panel_5.add(tfChat);
		tfChat.setColumns(40);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				sendMessage();
			}
		});
		panel_5.add(btnEnviar);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		try {
			textArea.setText(gerente.hello(email));
		} catch (RemoteException e) {
			JOptionPane.showMessageDialog(null, "Erro na sessão!", "Falhou!", JOptionPane.ERROR_MESSAGE);
			logout();
		}
		panel_4.add(textArea, BorderLayout.CENTER);
		textArea.setLineWrap(true);
	}
	
	private void logout() {
		setVisible(false);
		clientApp.finishChat();
	}
	
	private void sendMessage() {
		String message = tfChat.getText();
		textArea.setText(textArea.getText() + "\n'" + email + "' escreveu: " + message);
		try {
			String response = gerente.sendMessage(email, message);
			textArea.setText(textArea.getText() + "\n" + response);
		} catch (RemoteException e) {
			JOptionPane.showMessageDialog(null, "Erro na sessão!", "Falhou!", JOptionPane.ERROR_MESSAGE);
			logout();
		}
	}
}
