package server;

import java.util.ArrayList;

public class IA {
	private static final String DEFAULT_RESPONSE = "Não entendi sua pergunta. Por favor, tente outra.";
	public static final String DEFAULT_SALUTATION = "Meu nome é Sophia! Sou sua guia turística virtual no Brasil. Qual estado você deseja visitar?";
	private ArrayList<String> keys;
	private ArrayList<String> values;
	
	public IA() {
		keys = new ArrayList<>();
		values = new ArrayList<>();
		put("Bom dia", "Bom dia!");
		put("Bom dia.", "Bom dia!");
		put("Bom dia!", "Bom dia!");
		put("Boa noite", "Boa noite!");
		put("Boa noite.", "Boa noite!");
		put("Boa noite!", "Boa noite!");
		put("Boa tarde", "Boa tarde!");
		put("Boa tarde!", "Boa tarde!");
		put("Boa tarde!", "Boa tarde!");
		put("Boa noite", "Boa noite!");
		put("Tchau", "Tchau!");
		put("Obrigado", "De nada!");
		put("Obrigado!", "De nada!");
		put("Acre","No Acre, é indispensável um passeio ao Museu da Borracha, em Rio Branco. Conheça também o Calçadão da Gameleira e o Mercado Velho.");
		put("Alagoas","\"No caribe brasileiro\", seu passeio deve começar na praia de Pajuçara. De lá, o litoral alagoano reserva os mais belos aquários naturais do Brasil. Vale a pena dar um mergulho!");
		put("Amapá","No estado do meio do mundo, você poderá visitar o Marco Zero, onde há a divisão entre os hemisférios Norte e Sul.");
		put("Amapa","No estado do meio do mundo, você poderá visitar o Marco Zero, onde há a divisão entre os hemisférios Norte e Sul.");
		put("Amazonas", "No Amazonas, você pode visitar o Teatro Amazonas, localizado no Centro de Manaus, ou as cachoeiras da cidade de Presidente Figueiredo. Manaus também conta com passeio ao Encontro das Águas.");
		put("Bahia","A Bahia é o lugar da alegria! Lá você não pode deixar de pegar o Elevador Lacerda, passar na Basílica do Nosso Senhor do Bonfim e caminhar pelas ruas históricas do Pelourinho.");
		put("Brasília","Na capital federal, o palácio do Planalto, da Alvorada e o Memorial JK são boas pedidas.");
		put("Ceará","O turismo no Ceará começa na Praia do Futuro, cartão postal cearense. Outro ponto turístico é o Teatro José de Alencar");
		put("Ceara","O turismo no Ceará começa na Praia do Futuro, cartão postal cearense. Outro ponto turístico é o Teatro José de Alencar");
		put("Distrito Federal","Na capital federal, o palácio do Planalto, da Alvorada e o Memorial JK são boas pedidas.");
		put("Espírito Santo","No Espírito Santo, você não pode deixar de visitar o Convento da Penha, em Vila Velha, e para os aventureiros, o Pico da Bandeira. Na culinária, é obrigatório se deliciar com a Muqueca Capixaba.");
		put("Espirito Santo","No Espírito Santo, você não pode deixar de visitar o Convento da Penha, em Vila Velha, e para os aventureiros, o Pico da Bandeira. Na culinária, é obrigatório se deliciar com a Muqueca Capixaba.");
		put("Goiás","Goiás oferece muitas construções em estilo Art Déco. O MAC, museu de relevância, e as feiras tornarão sua viagem inesquecível.");
		put("Goias","Goiás oferece muitas construções em estilo Art Déco. O MAC, museu de relevância, e as feiras tornarão sua viagem inesquecível.");
		put("Maranhão","No Maranhão você pode visitar o Centro Histórico, que foi tombado pela UNESCO. Nele, o Palácio dos Leões e o Museu Histórico e Artístico se destacam.");
		put("Maranhao","No Maranhão você pode visitar o Centro Histórico, que foi tombado pela UNESCO. Nele, o Palácio dos Leões e o Museu Histórico e Artístico se destacam.");
		put("Mato Grosso do Sul","No Mato Grosso do Sul, você pode pegar o City Tour oficial da prefeitura, conhecer o Parque das Nações Indígenas e se deliciar com a gastronomia ímpar do estado.");
		put("Mato Grosso","No MT, a Chapada dos Guimarães é excelente para os aventureiros. Com grutas e cachoiras, é difícil aguentar apenas na parte urbana.");
		put("Minas Gerais","Em BH, vale apena visitar a Praça da Liberdade e o Complexo Arquitetônico da Pampulha. No interior, Ouro Preto também é uma boa opção.");
		put("Minas","Em BH, vale apena visitar a Praça da Liberdade e o Complexo Arquitetônico da Pampulha. No interior, Ouro Preto também é uma boa opção.");
		put("Paraíba","Na terra dos surfistas, o ideal é começar com as praias de Tambaú e Cabo Branco. O Farol do Cabo Branco também é uma boa opção.");
		put("Paraiba","Na terra dos surfistas, o ideal é começar com as praias de Tambaú e Cabo Branco. O Farol do Cabo Branco também é uma boa opção.");
		put("Paraná","Os paranaenses oferecem inúmeros parques com belíssimas paisagens. Faça também um passeio pelo Centro Histórico, conhecido como Largo da Ordem.");
		put("Parana","Os paranaenses oferecem inúmeros parques com belíssimas paisagens. Faça também um passeio pelo Centro Histórico, conhecido como Largo da Ordem.");
		put("Pará","No Pará, boas opções de turismo são o Teatro da Paz, a Estação das Docas, o Mercado Ver-o-Peso e a Catedral da Sé. E nada de sair de Belém sem degustar o famoso Pato no Tucupi!");
		put("Para","No Pará, boas opções de turismo são o Teatro da Paz, a Estação das Docas, o Mercado Ver-o-Peso e a Catedral da Sé. E nada de sair de Belém sem degustar o famoso Pato no Tucupi!");
		put("Pernambuco","Em Pernambuco, visite o Instituto Ricardo Brennand, um dos museus mais legais do país. Você também não pode perder a praia de Boa Viagem.");
		put("Piauí","O encontro dos rios Poti e Parnaíba são uma das atrações do Piauí. Experimente também o Capote, a Peixada Piratinga e o Arroz Maria Izabel da culinária piauiense.");
		put("Piaui","O encontro dos rios Poti e Parnaíba são uma das atrações do Piauí. Experimente também o Capote, a Peixada Piratinga e o Arroz Maria Izabel da culinária piauiense.");
		put("Rio grande do Sul","Na terra do chimarrão, você poderá provar essa bebida típica no Mercado Público da capital. Visite também o Museu de Ciência e Tecnologia da PUC-RS.");
		put("Rio Grande do Norte","No RN, vale a pena conhecer a Praia de Ponta Negra, o Morro do Careca e, principalmente, o Forte dos Reis Magos.");
		put("Rio de Janeiro","No Rio, não pode faltar a foto no Cristo Redentor e a visita à Praia de Cabo Frio.");
		put("Rondônia","Na Capital, Porto Velho, você poderá visitar o Museu Ferroviário e a Praça das 3 Caixas D'Água.");
		put("Rondonia","Na Capital, Porto Velho, você poderá visitar o Museu Ferroviário e a Praça das 3 Caixas D'Água.");
		put("Roraima","Em Roraima, uma boa opção é o Turismo Ecológico, com trilhas, praias e escaladas. Na Orla Taumanan, você poderá ter contato com a cultura e culinária regional.");
		put("São Paulo","Em Sampa, visite a Av. Paulista, as exposições do MASP e o Museu do Futebol no estádio do Pacaembú.");
		put("Sampa","Em Sampa, visite a Av. Paulista, as exposições do MASP e o Museu do Futebol no estádio do Pacaembú.");
		put("Santa Catarina","Em SC você pode conhecer a praia da Joaquina e se divertir nas dunas do local.");
		put("Sergipe","Em Sergipe, a passarela do Caranguejo oferece o melhor da culinária local. O estado se destaca também por oferecer bons preços para os turistas. A praia de Atalaia é outra excelente opção.");
		put("Tocantins","Em Tocantins, vale a pena visitar o Parque Cesamar. Para os amantes de arquitetura, é possível prestigiar o Memorial Luis Carlos Prestes, projetado por Oscar Niemeyer.");
		put("Rio","No Rio, não pode faltar a foto no Cristo Redentor e a visita à Praia de Cabo Frio.");
	}
	
	public void put(String key, String value) {
		keys.add(key);
		values.add(value);
	}
	
	public String answer(String question) {
		for(int i = 0; i < keys.size(); i++) {
			String key = keys.get(i); 
			if(key.equalsIgnoreCase(question) || question.toLowerCase().contains(key.toLowerCase())) {
				return values.get(i);
			}
		}
		
		return DEFAULT_RESPONSE;
	}
}
