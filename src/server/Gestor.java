package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

import client.Mensageiro;

public class Gestor extends UnicastRemoteObject implements Mensageiro {
	private static final long serialVersionUID = 1L;
	private ArrayList<User> users;
	private IA sophia;

	public Gestor() throws RemoteException {
		super();
		users = new ArrayList<>();
		sophia = new IA();
	}

	@Override
	public String hello(String email) throws RemoteException {
		return "'Sophia' escreveu: Olá, " + email + "! " + IA.DEFAULT_SALUTATION;
	}

	@Override
	public boolean register(String email) throws RemoteException {
		User user = getUser(email);
		
		if(user == null) {
			user = new User();
			user.email = email;
			user.messages = new ArrayList<>();
			users.add(user);
			return false;
		}
		
		return true;
	}

	private User getUser(String email) {
		for(User user:users) {
			if(user.email.equalsIgnoreCase(email)) {
				return user;
			}
		}
		
		return null;
	}

	@Override
	public String sendMessage(String email, String message) throws RemoteException {
		User user = null;
		for(User u:users) {
			if(u.email.equalsIgnoreCase(email)) {
				user = u;
				user.messages.add(email + " escreveu: " + message);
				String response = "'Sophia' escreveu: " + sophia.answer(message);
				user.messages.add(response);
				return response;
			}
		}
		return "Erro na Sessão!";
	}
	
	@Override
	public ArrayList<String> getHistoric(String email) throws RemoteException {
		for(User u:users) {
			if(u.email.equalsIgnoreCase(email)) {
				return u.messages;
			}
		}
		return null;
	}
	
	private class User {
		private String email;
		private ArrayList<String> messages;
	}
}
