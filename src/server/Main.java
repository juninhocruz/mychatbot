package server;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Main {

	public static void main(String[] args) throws RemoteException, MalformedURLException, AlreadyBoundException {
		Gestor gestor = new Gestor();
		
		String host = "MyChatBot";
		int port = 8080;
		
		System.setProperty("java.rmi.server.hostname","192.168.0.107");
		
		System.out.print("Iniciando servidor na porta " + port + "... ");
		Registry registry = LocateRegistry.createRegistry(port);
		registry.bind(host, gestor);
		System.out.print("OK!");
	}

}
